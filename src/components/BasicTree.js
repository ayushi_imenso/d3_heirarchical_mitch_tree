import React, { useRef, useEffect, useState } from 'react';
import { saveAs } from 'file-saver';
import html2canvas from 'html2canvas';
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { boxedTree } from 'd3-mitch-tree';
import 'd3-mitch-tree/dist/css/d3-mitch-tree-theme-default.min.css';
import 'd3-mitch-tree/dist/css/d3-mitch-tree.min.css';
import GetHierarchicalData from './getHierarchicalData';
import axios from 'axios';

const MyComponent = () => {
    const nodeData = JSON.parse(localStorage.getItem("rootData"));
    const data = GetHierarchicalData();
    const visualisationRef = useRef(null);
    const [orientation, setOrientation] = useState('topToBottom');
    const [nodeDistance, setNodeDistance] = useState(150);
    const [copyData, setCopyData] = useState([data]);

    const prepareData = (data) => {
        let Arr = [];
        data.map((item) => {
            Arr.push(item.chartdatas);
        });
        return Arr;
    };


    // const showData = () => {
    //     console.log("inside mitch copydata", copyData)
    //     const treePlugin = new boxedTree()
    //         .setData(copyData)
    //         .setAllowFocus(false)
    //         .setElement(visualisationRef.current)
    //         .setIdAccessor((data) => data.id)
    //         .setChildrenAccessor((data) => data[0].children)
    //         .setBodyDisplayTextAccessor((data) => data[0]?.description)
    //         .setTitleDisplayTextAccessor((data) => data[0]?.name)

    //         .on("nodeClick", (node) => {
    //             console.log(node);
    //             if (node.type === "expand") {
    //                 let params = { _id: data[0]?.id };
    //                 axios.post('http://peak-discount-api.imenso.in:5019/charts/getChartDatas', params)
    //                     .then((response) => {
    //                         let records = prepareData(response.data.data);
    //                         console.log("rw", data);
    //                         // setCopyData((data) => ({
    //                         //     ...data,
    //                         //     children: records
    //                         // }));
    //                         let Arr = []
    //                         copyData[0].children = records
    //                         // setCopyData({ ...copyData, children: records });
    //                         console.log("Arr", copyData);

    //                     })
    //                     .catch((error) => {
    //                         console.log("err", error);
    //                     });
    //                 console.log("kk", copyData);
    //                 treePlugin.setData(copyData);
    //             }

    //             else if (node.type === "collapse") {
    //                 setCopyData({
    //                     ...copyData,
    //                     children: []
    //                 })
    //             }
    //         })
    //         .setNodeDepthMultiplier(nodeDistance)
    //         .setOrientation(orientation)
    //         .setWidthWithoutMargins(1000)
    //         .setHeightWithoutMargins(500)
    //         .getNodeSettings()
    //         .setSizingMode('nodesize')
    //         .setVerticalSpacing(5)
    //         .setHorizontalSpacing(5)
    //         .back()
    //         .initialize();
    //     treePlugin.getZoomListener().scaleTo(treePlugin.getSvg(), 0.5);

    //     const rootNode = visualisationRef.current.querySelector('.mitch-d3-tree.boxed-tree.default .node .title-group .title-box');
    //     if (rootNode) {
    //         rootNode.style.fill = data[0]?.color;
    //     }

    //     // Update the children property of the root node
    //     const rootNodeData = treePlugin.getRoot().data;
    //     if (rootNodeData.id === "root") {
    //         rootNodeData.children = data[0].children;
    //         treePlugin.update(treePlugin.getRoot());
    //     }
    // };


    const generateMitchTree = () => {
        console.log("inside mitch copydata", copyData);
        const treePlugin = new boxedTree()
            .setData(copyData)
            .setAllowFocus(false)
            .setElement(visualisationRef.current)
            .setIdAccessor((data) => data.id)
            .setChildrenAccessor((data) => data[0].children)
            .setBodyDisplayTextAccessor((data) => data[0]?.description)
            .setTitleDisplayTextAccessor((data) => data[0]?.name)
            .on("nodeClick", (node) => {
                console.log(node);
                if (node.type === "expand") {
                    let params = { _id: data[0]?.id };
                    axios
                        .post('http://peak-discount-api.imenso.in:5019/charts/getChartDatas', params)
                        .then((response) => {
                            let records = prepareData(response.data.data);
                            console.log("data", data);
                            copyData[0].children = records;
                            console.log("Arr", copyData);
                            treePlugin.setChildrenAccessor(records)
                            treePlugin.setData(copyData);
                            treePlugin.update(treePlugin.getRoot());
                        })
                        .catch((error) => {
                            console.log("err", error);
                        });
                } else if (node.type === "collapse") {
                    let updatedCopyData = { ...copyData, children: [] };
                    setCopyData(updatedCopyData);
                    treePlugin.setData(updatedCopyData);
                    treePlugin.update(treePlugin.getRoot());
                }
            })
            .setNodeDepthMultiplier(nodeDistance)
            .setOrientation(orientation)
            .setWidthWithoutMargins(1000)
            .setHeightWithoutMargins(500)
            .getNodeSettings()
            .setSizingMode('nodesize')
            .setVerticalSpacing(5)
            .setHorizontalSpacing(5)
            .back()
            .initialize();

        treePlugin.getZoomListener().scaleTo(treePlugin.getSvg(), 0.5);

        const rootNode = visualisationRef.current.querySelector('.mitch-d3-tree.boxed-tree.default .node .title-group .title-box');
        if (rootNode) {
            rootNode.style.fill = data[0]?.color;
        }

        // Update the children property of the root node
        const rootNodeData = treePlugin.getRoot().data;
        if (rootNodeData.id === "root") {
            rootNodeData.children = data[0].children;
            treePlugin.update(treePlugin.getRoot());
        }
    };


    const verticalOrientation = () => {
        setOrientation('topToBottom');
        setNodeDistance(150);
    };

    const horizontalOrientation = () => {
        setOrientation('leftToRight');
        setNodeDistance(270);
    };


    // const callfun = () => {
    //     showData()
    // }


    const onClickHandler = () => {
        const visualisationNode = visualisationRef.current;
        window.scrollTo(0, 0);
        html2canvas(visualisationNode, { scrollY: -window.scrollY }).then((canvas) => {
            canvas.toBlob((blob) => {
                saveAs(blob, 'mitch_tree.png');
            });
        });
    };


    useEffect(() => {
        if (!data) {
            return;
        }
        setCopyData(data);
        generateMitchTree(data);
    }, [data, copyData, orientation, nodeDistance]);


    return (
        <div>
            {/* <button onClick={callfun}>click</button> */}
            <Button className="btn btn-primary" color="primary" onClick={onClickHandler}>
                Download as PNG
            </Button>{' '}
            <Button color="primary" outline onClick={horizontalOrientation}>
                Horizontal View
            </Button>{' '}
            <Button color="primary" outline onClick={verticalOrientation}>
                Vertical View
            </Button>
            <div ref={visualisationRef} id="visualisation" className='tree-container'></div>
        </div>
    );
};

export default MyComponent;








