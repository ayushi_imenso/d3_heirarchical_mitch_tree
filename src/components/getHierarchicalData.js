import { useEffect, useState } from 'react';
import axios from 'axios';

const GetHierarchicalData = () => {
    const [data, setData] = useState([{}]);
    const getData = () => {
        let params = { _id: "" };
        axios.post('http://peak-discount-api.imenso.in:5019/charts/getChartDatas', params)
            .then((response) => {
                let values = response.data.data.map((data) => ({ ...data.chartdatas,children : []}));
                setData({
                    ...values
                });
                localStorage.setItem("rootData", JSON.stringify(...values));
            })
            .catch((error) => {
                console.log("err", error);
            });
    };

    useEffect(() => {
    }, [data]);


    useEffect(() => {
        getData();
    }, []);

    return data;
};

export default GetHierarchicalData;
