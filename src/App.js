import React from "react";
import BasicTree from "./components/BasicTree";
import getHierarchicalData from "./components/getHierarchicalData";
import "./components/styles.css";

function App() {
  let data = getHierarchicalData();
  return (
    <div className="App">
      <BasicTree data={data} />
    </div>
  );
}

export default App;

